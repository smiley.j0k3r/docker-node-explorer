#!/bin/sh

sudo docker run -d --name node_exporter  --net="host"   --pid="host"   --restart=always -v "/:/host:ro,rslave"   quay.io/prometheus/node-exporter:latest   --path.rootfs=/host
